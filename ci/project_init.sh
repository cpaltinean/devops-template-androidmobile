#!/bin/bash

echo "Init project settings: load and set-up variable ..."

# we load project settings "ci/project.settings"
source ci/project.settings

# give execution right to proper files (need to take in consideration if some path are or not empty)
chmod +x $project_path/gradlew
chmod +x ci/*.sh
# add scripts path to system path
PATH=$base_path/ci/:$PATH

# set current date
curent_date=$(date +"%Y-%m-%d")
report_date=$(date -d "+1 days" +"%Y-%m-%d")

# load acive version !!! not the same with new_version
source version_reader.sh

# save variable to a temp file (.variables) to be loaded to "after_script" section
echo 'export base_path="'$base_path'"' >> .variables
echo 'export project_path="'$project_path'"' >> .variables
echo 'export project_app_path="'$project_app_path'"' >> .variables
echo 'export project_apk_path="'$project_apk_path'"' >> .variables
echo 'export google_services_path="'$google_services_path'"' >> .variables
