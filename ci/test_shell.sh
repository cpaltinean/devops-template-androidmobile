#!/usr/bin/env bash

function echoFooBar {
  echo "start"
  echo
  sleep 40
  echo "end"
}

function adevarat {
  project_path=$(pwd)
  build_gradle_path="$(pwd)/build.gradle"

  while IFS= read -r line; do
    line_version_code=${line#*"VERSION_CODE="}
    if [ "$line_version_code" != "$line" ] ; then
      version_code=$(echo "$line_version_code" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//')
      echo "$version_code"
    fi
    line_version_name=${line#*"VERSION_NAME="}
    if [ "$line_version_name" != "$line" ] ; then
      version_name=$(echo "$line_version_name" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//')
      echo "$version_name"
    fi
  done < "$build_gradle_path"
}
