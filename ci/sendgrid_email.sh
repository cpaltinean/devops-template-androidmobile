#!/bin/bash

api_url="https://api.sendgrid.com/v3/mail/send"
api_key="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"

dev_internal_email="calin.paltinean@3ss.tv"
devops_service_email="devops-service@3ss.tv"

# shellcheck disable=SC2154
subject="Release $new_version"
content="Hi all,<br>
<br>
Here is TV app build $new_version:
<ul>
<li>Build $new_version: $confluence_release_attachments/$confluence_release_page_id/$file_to_upload?api=v2</li>
<li>Releases: $confluence_release_page</li>
</ul>
Best regards,<br>
TV app team"

content=$(echo "$content" | tr -d '\n')

# shellcheck disable=SC2154
# shellcheck disable=SC2034
queryResult=$(curl --silent --request POST --url "$api_url" --header "Authorization: Bearer $api_key" --header 'Content-Type: application/json' --data '{"personalizations": [{"to": [{"email": "'$dev_internal_email'"}]}], "from": {"email": "'$devops_service_email'"}, "subject":"'"$subject"'", "content": [{"type": "text/html", "value": "'"$content"'"}]}')
