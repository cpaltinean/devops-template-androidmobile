#!/usr/bin/env bash

# load version from "version_keeper.json" this have to be the same with "build.gradle"
active_version=$(perl -nle'print $& while m{(?<="version": ")[^"]*}g' $base_path/ci/version_keeper.json)
