#!/bin/bash

function jira_create_version()
{
  echo "Create new Release version on JIRA ..."

  new_version_ios="$new_version-iOS"
  atlassian_query_create_version="$jira_api_url/version"
  query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X POST -d '{"description":"","name": "'$new_version_ios'","project": "'$jira_project_key'","archived": false,"released": false}' -H "Content-Type: application/json" $atlassian_query_create_version)
}

# parse the response json and fill all needed array
function getDataFromQueryResult()
{
  page_key=();
  page_status=();

  # clean text until repetitive information (what we need)
  string_data=${query_result#*"[{"}
  string_data=${string_data%"}]"*}
  # add split string at end of string, for proper process
  string_data=$string_data$"},{"

  start_with="\"expand\""
  if [[ $string_data = $start_with* ]]; then
    while [[ $string_data ]]; do
      temp_info="${string_data%%",{"*}"
      # parse section and get all information
      # key's
      temp_key_data=${temp_info#*"key\":\""}
      temp_key_data=${temp_key_data%"\",\"fields"*}
      page_key+=( $temp_key_data );

      # status
      temp_page_status=${temp_info#*"png\",\"name\":\""}
      temp_page_status=$(echo $temp_page_status | awk -F "\",\"id" '{ print $1 }')
      page_status+=( "$temp_page_status" );

      string_data=${string_data#*",{"};
    done;
  fi
}

function jira_update_issues()
{
  echo "Update label field to active version on Jira tasks, bugs, story ..."

  new_version_ios="$new_version-iOS"
  have_data=1
  while [ $have_data = 1 ]; do
    have_data=0

    atlassian_query="$jira_api_url/search/?$(date +%s)"
    atlassian_data='{"jql":"project = '$jira_project_key' AND issuetype != Bug AND status in (Resolved, Accepted) AND affectedVersion is EMPTY AND fixVersion is EMPTY","maxResults":20,"fields":["key"]}'
    query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X POST --data "$atlassian_data" -H 'Content-Type: application/json' $atlassian_query)
    echo "Search result for non-bug issues:"
    echo "$query_result"

    page_key=($(echo $(echo $query_result | awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'key'\042/){print $(i+1)}}}' | tr -d '"') | tr " " "\n"))
    for index in "${!page_key[@]}"
    do
      have_data=1
      atlassian_query="$jira_api_url/issue/${page_key[index]}"
      echo "$atlassian_query"

      atlassian_data='{"update": {"fixVersions": [{"add": {"name": "'$new_version_ios'"}}]}}'
      query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X PUT --data "$atlassian_data" -H 'Content-Type: application/json' $atlassian_query)
      echo "${page_key[index]} issue updated"

      sleep 1
    done
    sleep 1
  done

  have_data=1
  while [ $have_data = 1 ]; do
    have_data=0

    atlassian_query="$jira_api_url/search/?$(date +%s)"
    atlassian_data='{"jql":"project = '$jira_project_key' AND issuetype = Bug AND (affectedVersion not in (\"'$new_version_ios'\") OR affectedVersion is EMPTY) AND fixVersion is EMPTY","maxResults":20,"fields":["key","status"]}'
    query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X POST --data "$atlassian_data" -H 'Content-Type: application/json' $atlassian_query)
    echo "Search result for bug issues:"
    echo "$query_result"

    # parse the result json and start to update
    getDataFromQueryResult

    for index in "${!page_key[@]}"
    do
      have_data=1
      atlassian_query="$jira_api_url/issue/${page_key[index]}"
      echo "$atlassian_query"

      if [ "${page_status[index]}" == "Resolved" ] ||  [ "${page_status[index]}" == "Accepted" ]; then
        atlassian_data='{"update": {"fixVersions": [{"add": {"name": "'$new_version_ios'"}}]}}'
      else
        atlassian_data='{"update": {"versions": [{"add": {"name": "'$new_version_ios'"}}]}}'
      fi
      query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X PUT --data "$atlassian_data" -H 'Content-Type: application/json' $atlassian_query)
      echo "${page_key[index]} bug updated"

      sleep 1
    done
    sleep 1
  done
}

function jira_update_version_status
{
  active_branch=$CI_COMMIT_REF_NAME
  if [[ $active_branch != "release/"* ]]; then
    echo "Active branch should be a release branch"
    exit 1
  fi

  release_branch_nr=${active_branch#*"release/"}

  atlassian_query="$jira_api_url/project/$jira_project_key/versions"
  query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' $atlassian_query)

  versionId=""
  versionName=""
  versionNamePrev=""
  result=0
  userReleaseDate=$(date +%d)/$(date +%b)/$(date +%Y)

  while [ $result -eq 0 ]
  do
    versionId=${query_result#*"\"id\":\""}
    versionName=${query_result#*"\"name\":\""}
    query_result=$versionName

    versionName=$(echo "$versionName" | cut -d'"' -f 1)
    if [[ "$versionName" == "$versionNamePrev" ]]; then
      result=1
    else
      versionNamePrev=$versionName

      IFS='.' read -a version_name_parts <<< "$versionName"
      major=${version_name_parts[0]}
      release=${version_name_parts[1]}
      develop=${version_name_parts[3]}

      IFS='.' read -a release_branch_parts <<< "$release_branch_nr"
      majorArg=${release_branch_parts[0]}
      releaseArg=${release_branch_parts[1]}

      platform=""
      IFS='-' read -a develop_parts <<< "$develop"
      if [[ ${#develop_parts[@]} == 2 ]]; then
        platform=${develop_parts[1]}
      fi

      if [ "$major" == "$majorArg" ] && [ "$release" == "$releaseArg" ] && [ "$platform" == "iOS" ]; then
        versionId=$(echo "$versionId" | cut -d'"' -f 1)

        atlassian_query="$jira_api_url/version/$versionId"
        atlassian_data='{"userReleaseDate": "'$userReleaseDate'", "released": true}'
        query_result_released=$(curl --silent -u $atlassian_login:$atlassian_password -X PUT --data "$atlassian_data" -H 'Content-Type: application/json' $atlassian_query)
      fi
    fi
  done
}
