#!/bin/bash

echo "Close release branch ..."

branch_name=$CI_COMMIT_REF_NAME

cd ~
mkdir temp
cd temp

# get repository url
# update the origin repository’s master branch with the release branch
# create tag on master branch
# replace only versioning files on the origin repository’s develop branch with versioning files from release branch
# push everything on upstream
IFS='@' read -a http_parts <<< "$CI_REPOSITORY_URL"
git config --global user.email "noreply@3ss.tv"
git config --global user.name "CI/CD Bot"

git clone --branch $branch_name "https://USERNAME:${gitlab_source_token}@"${http_parts[1]}
cd $(find . -mindepth 1 -print -quit)
git pull

real_version=$(perl -nle'print $& while m{(?<="version": ")[^"]*}g' "$(pwd)/ci/version_keeper.json")

build_gradle_path="$(pwd)/build.gradle"
while IFS= read -r line; do
  line_version_code=${line#*"VERSION_CODE="}
  if [ "$line_version_code" != "$line" ] ; then
    version_code=$(echo "$line_version_code" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//')
  fi
  line_version_name=${line#*"VERSION_NAME="}
  if [ "$line_version_name" != "$line" ] ; then
    version_name=$(echo "$line_version_name" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//')
  fi
done < "$build_gradle_path"

git checkout master
git rm -r '*'
git checkout "$branch_name" .
git commit -am "$real_version Released"
git tag -a "${real_version}" -m "CI/CD automatic release version $real_version"
git push origin --tags
git push origin master

git checkout develop
perl -pi -e 's/VERSION_CODE=.*/VERSION_CODE= '$version_code'/g' build.gradle
perl -pi -e 's/VERSION_NAME=.*/VERSION_NAME= '$version_name'/g' build.gradle
perl -pi -e 's/"version": .*/"version": '$real_version'/g' ci/version_keeper.json
git add ci/version_keeper.json build.gradle
git commit -m "[skip ci] Bump version to $real_version"

git branch -d "$branch_name"

git push origin develop
git push origin :"$branch_name"
