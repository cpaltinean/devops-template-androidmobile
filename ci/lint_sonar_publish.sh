#!/bin/bash

# this is publish to Sonarqube process
echo "Start lint process and report the result to SonarQube server ..."

# Build command
command="./gradlew sonarqube"
# Add exclude path and files
#command="$command -Dsonar.exclusions=ci/**/*,fastlane/**/*,**/*.png,**/*.yml"
# SonarQube server set-up information
command="$command -Dsonar.host.url=http://$sonarqube_url"
command="$command -Dsonar.login=$sonarqube_login"
command="$command -Dsonar.password=$sonarqube_password"
# Project name from SonarQube server (not mandatory)
command="$command -Dsonar.projectName=$project_sonar_name"
# Project key that map the project with SonarQube project
command="$command -Dsonar.projectKey=$project_sonar_key"
# SonarQube scanner set-up information
command="$command -Dsonar.sources=$project_sonar_source"
# Set to publish mode sonarqube
command="$command -Dsonar.analysis.mode=publish"
# Debug mode - try to find errors (not mandatory) - oter option (--scan,  -debug)
#command="$command -stacktrace"
echo $command
# For some projects we need to exclude some tasks (usually this issues have to fix by project team)
# -x <task_name>

# change to project directory (posible base path)
cd $project_path
# Run command
eval $command
# change to base path (posible project directory)
cd $base_path
