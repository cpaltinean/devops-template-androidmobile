#!/usr/bin/env bash

# this we use on gitlab CI process to commit new app version info (we need to stop CI flow)
echo "The version has changed, we update the version files in the repository ..."

# get "git" path and take only what we need
IFS='@' read -a http_parts <<< "$CI_REPOSITORY_URL"

git remote set-url origin "https://USERNAME:${gitlab_source_token}@"${http_parts[1]}
git config --global user.email "noreply@3ss.tv"
git config --global user.name "CI/CD Bot"

git add $base_path/ci/version_keeper.json $project_path/build.gradle

active_version=$(perl -nle'print $& while m{(?<="version": ")[^"]*}g' $base_path/ci/version_keeper.json)
git commit -m "[skip ci] Bumped to $active_version"
git push origin HEAD:$CI_COMMIT_REF_NAME
