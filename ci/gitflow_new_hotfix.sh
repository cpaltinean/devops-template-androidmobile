#!/bin/bash

echo "Start new hotfix branch ..."

# calculate the new hotfix number
source version_reader.sh
IFS='.' read -a version_parts <<< "$active_version"
branch_name="hotfix/${version_parts[0]}.${version_parts[1]}.$((${version_parts[2]} + 1)).0"

cd ~
mkdir temp
cd temp

# get repository url, create new hotfix branch and commit this
IFS='@' read -a http_parts <<< "$CI_REPOSITORY_URL"
git config --global user.email "noreply@3ss.tv"
git config --global user.name "CI/CD Bot"

git clone "https://USERNAME:${gitlab_source_token}@"${http_parts[1]}
cd $(find . -mindepth 1 -print -quit)

# fetch the tags to the local repository
git fetch --all --tags --prune
# check out the tag and create a new hotfix branch
git checkout "tags/${active_version}" -b "$branch_name"
git branch

# update the version files in the repository
source version_manager.sh
version_manager

git add -A && git commit -m "Commit new branch ${branch_name}"
git push origin "$branch_name"
