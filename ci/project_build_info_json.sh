#!/usr/bin/env bash

active_version=$(perl -nle'print $& while m{(?<="version": ")[^"]*}g' $base_path/ci/version_keeper.json)

gradle_app_path="$project_app_path/build.gradle"
file_content=$(cat "$gradle_app_path")

applicationId=${file_content#*"applicationId"}

IFS=' ' read -a parts <<< "$applicationId"
applicationId=${parts[0]}

applicationId=$(echo "${applicationId}" | sed 's/^.//')
applicationId=$(echo "${applicationId}" | sed 's/.$//')

file_to_upload_path="$(ls $project_apk_path/*.apk | head -1)"
file_to_upload=$(basename -- "$file_to_upload_path")

json_content='{
 "name": "LauncherApp-'$active_version'",
 "package": "'$applicationId'",
 "version": "'$new_version_value'",
 "requiredAndroidVersion": '$req_android_version',
 "url": "'$mcdn_tv_comhem_url''$file_to_upload'"
}'

build_info_file_path=$base_path/build_info.json

cat > $build_info_file_path <<< "$json_content"
