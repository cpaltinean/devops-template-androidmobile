#!/bin/bash

echo "Update release confluence page ..."

# upload a file to confluence
file_to_upload_path="$(ls $project_apk_path/*.apk | head -1)"
file_to_upload=$(basename -- "$file_to_upload_path")

# so what we do here.
# find our page base on confluence page id
# upload the new attachment
# upadate the page with new information

#get the page from confluence
confluence_query="$confluence_url/content/$confluence_release_page_id?expand=body.storage,version"
query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' --url $confluence_query)

#get page version
page_version=${query_result#*"number\":"}
page_version=${page_version%",\"minorEdit"*}
#get page body
page_body=${query_result#*"value\":\""}
page_body=${page_body%"\",\"representation"*}
#calculate new page version
if [ -n "$page_version" ]; then
  page_version=$((page_version + 1))
else
  page_version=1
fi

# upload the new attachment
confluence_query="$confluence_url/content/$confluence_release_page_id/child/attachment"
query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X POST -H 'X-Atlassian-Token: nocheck' -F "file=@$file_to_upload_path" $confluence_query)

new_version_ios="$new_version-iOS"

# create a new page with nee attachment
action_date=$(date +"%Y-%m-%d %H:%M:%S")
confluence_body="
<table class='wrapped relative-table' style='width: 80%;'>
<colgroup><col style='width: 8.0%;' /><col style='width: 30.0%;' /><col style='width: 45.0%;' /><col style='width: 12.0%;' /><col /></colgroup>
<thead><tr><th>Version</th><th>Creation page</th><th>Apk file</th><th>Status</th><th>Date</th></tr></thead>
<tbody><tr>
<td>$new_version</td>
<td>$action_date GMT</td>
<td><ac:link><ri:attachment ri:filename='$file_to_upload' /></ac:link></td>
<td><ac:structured-macro ac:name='status' ac:schema-version='1' ac:macro-id='104f3edd-79c0-4e6d-87be-64e05c84f14e'><ac:parameter ac:name='title'>released</ac:parameter><ac:parameter ac:name='colour'>Yellow</ac:parameter></ac:structured-macro></td>
<td>N/A</td>
</tr></tbody></table>
<p> </p>
<p><strong>$confluence_space All => Status: Resolved + Accepted</strong>
<ac:structured-macro ac:name='jira' ac:schema-version='1'>
<ac:parameter ac:name='server'>System JIRA</ac:parameter>
<ac:parameter ac:name='jqlQuery'>project = '$jira_project_key' AND status in (Resolved, Accepted) AND fixVersion = $new_version_ios ORDER BY priority DESC, updated DESC</ac:parameter>
<ac:parameter ac:name='serverId'>$atlassian_jira_server</ac:parameter>
</ac:structured-macro>
</p><p>
<strong>$confluence_space All Bugs => Status: NOT Resolved (Known Issues)</strong>
<ac:structured-macro ac:name='jira' ac:schema-version='1'>
<ac:parameter ac:name='server'>System JIRA</ac:parameter>
<ac:parameter ac:name='jqlQuery'>project = '$jira_project_key' AND issuetype = Bug AND affectedVersion = $new_version_ios AND created &lt; $report_date ORDER BY priority DESC, updated DESC</ac:parameter>
<ac:parameter ac:name='serverId'>$atlassian_jira_server</ac:parameter>
</ac:structured-macro>
</p><br />"

# clean body to be a proper data
confluence_body="$confluence_body $page_body"
confluence_query="$confluence_url/content/$confluence_release_page_id"
confluence_body=$(echo $confluence_body | tr -d '\n')
confluence_data='{"id":"'$confluence_release_page_id'","type":"page","title":"'$confluence_release_page_name'","body":{"storage":{"value":"'$confluence_body'","representation":"storage"}},"version":{"number":'$page_version'}}'
query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X PUT -H 'Content-Type: application/json' -d "$confluence_data" $confluence_query)
