#!/bin/bash

echo "Close hotfix branch ..."

branch_name=$CI_COMMIT_REF_NAME

cd ~
mkdir temp
cd temp

# get repository url
# update the origin repository’s master branch with the hotfix branch
# create tag on master branch
# push everything on upstream
IFS='@' read -a http_parts <<< "$CI_REPOSITORY_URL"
git config --global user.email "noreply@3ss.tv"
git config --global user.name "CI/CD Bot"

git clone --branch $branch_name "https://USERNAME:${gitlab_source_token}@"${http_parts[1]}
cd $(find . -mindepth 1 -print -quit)
git pull

real_version=$(perl -nle'print $& while m{(?<="version": ")[^"]*}g' "$(pwd)/ci/version_keeper.json")

git checkout master
git rm -r '*'
git checkout "$branch_name" .
git commit -am "$real_version Released"
git tag -a "${branch_name#*/}" -m "CI/CD automatic hotfix version $real_version"
git push origin --tags
git push origin master

# delete local hotfix branch
git branch -d "$branch_name"

# delete remote hotfix branch
git push origin :"$branch_name"
