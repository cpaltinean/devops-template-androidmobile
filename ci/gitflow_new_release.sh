#!/bin/bash

echo "Start new release branch ..."

cd ~
mkdir temp
cd temp

# get repository url, create new release branch and commit this
IFS='@' read -a http_parts <<< "$CI_REPOSITORY_URL"
git config --global user.email "noreply@3ss.tv"
git config --global user.name "CI/CD Bot"

git clone "https://USERNAME:${gitlab_source_token}@"${http_parts[1]}
cd $(find . -mindepth 1 -print -quit)
git checkout develop

real_version=$(perl -nle'print $& while m{(?<="version": ")[^"]*}g' "$(pwd)/ci/version_keeper.json")

# calculate the new release number
IFS='.' read -a version_parts <<< "$real_version"
branch_name="release/${version_parts[0]}.$((${version_parts[1]} + 1)).0.0"

git checkout -b "$branch_name" develop
git branch
echo "${branch_name}" >> branch_name.txt
git add -A && git commit -m "Commit new branch ${branch_name}"
git push origin "$branch_name"
