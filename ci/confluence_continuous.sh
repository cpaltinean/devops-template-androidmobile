#!/bin/bash

echo "Update continuous deployment confluence page ..."

# upload a file to confluence
file_to_upload_path="$(ls $project_apk_path/*.apk | head -1)"
file_to_upload=$(basename -- "$file_to_upload_path")

# so what we do here.
# find our page base on confluence page id
# fint attachment id and delete, purge the attachment
# delete, purge the page base on id
# upload the new attachment and create a new page

#get the page from confluence
confluence_query="$confluence_url/content/$confluence_continuous_page_id?expand=body.storage,version"
query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' --url $confluence_query)

#get page version
page_version=${query_result#*"number\":"}
page_version=${page_version%",\"minorEdit"*}
#get page body
page_body=${query_result#*"value\":\""}
page_body=${page_body%"\",\"representation"*}
#calculate new page version
if [ -n "$page_version" ]; then
  page_version=$((page_version + 1))
else
  page_version=1
fi
#get attachment name from page_body
attachment_name=${page_body#*"ri:filename=\\\""}
attachment_name=${attachment_name%"\\\" ri:version-at-save=\\\"1\\\" /></ac:link>"*}

# search for attachment id <this attachment is use only one time on this parent> for deleted
if [ -n "$attachment_name" ]; then
  confluence_query="$confluence_url/content/$confluence_continuous_page_id/child/attachment"
  query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' --url $confluence_query)
  array_of_id=($(echo $(echo $query_result | awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'id'\042/){print $(i+1)}}}' | tr -d '"') | tr " " "\n"))
  array_of_name=($(echo $(echo $query_result | awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'title'\042/){print $(i+1)}}}' | tr -d '"') | tr " " "\n"))
  for index in "${!array_of_id[@]}"
  do
    if [ "${array_of_name[index]}" == "$attachment_name" ]; then
      attachment_id=${array_of_id[index]}
      break
    fi
  done
fi

# delete, purge the attachment
if [ -n "$attachment_id" ]; then
  confluence_query="$confluence_url/content/$attachment_id"
  query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X DELETE -H 'Accept: application/json' -H 'Content-Type: application/json' $confluence_query)
  confluence_query="$confluence_url/content/$attachment_id?status=trashed"
  query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X DELETE -H 'Accept: application/json' -H 'Content-Type: application/json' $confluence_query)
fi

# upload the new attachment
confluence_query="$confluence_url/content/$confluence_continuous_page_id/child/attachment"
query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X POST -H 'X-Atlassian-Token: nocheck' -F "file=@$version_info_file_path" $confluence_query)

# create a new page with nee attachment
action_date=$(date +"%Y-%m-%d %H:%M:%S")
confluence_body="
<table class='wrapped relative-table' style='width: 50%;'>
<thead><tr><th>Creation page</th><th>Apk file</th></tr></thead>
<tbody><tr>
<td>$action_date GMT</td>
<td><ac:link><ri:attachment ri:filename='$version_info_file' /></ac:link></td>
</tr></tbody></table>
<p> </p>
<p><strong>$confluence_space All => Status: Resolved (Current Status)</strong>
<ac:structured-macro ac:name='jira' ac:schema-version='1' data-layout='full-width' ac:macro-id='bf168246-a871-486d-8702-70cb3e95c5a5'>
<ac:parameter ac:name='server'>System JIRA</ac:parameter>
<ac:parameter ac:name='maximumIssues'>50</ac:parameter>
<ac:parameter ac:name='columns'>key,summary,type,created,updated,due,assignee,reporter,priority,status,resolution,severity</ac:parameter>
<ac:parameter ac:name='jqlQuery'>project = '$jira_project_key' AND status in (Resolved, Accepted) ORDER BY priority DESC, updated DESC</ac:parameter>
<ac:parameter ac:name='serverId'>$atlassian_jira_server</ac:parameter>
<ac:parameter ac:name='cache'>off</ac:parameter>
</ac:structured-macro>
</p><p>
<strong>$confluence_space All Bugs => Status: NOT Resolved (Known Issues)</strong>
<ac:structured-macro ac:name='jira' ac:schema-version='1'>
<ac:parameter ac:name='maximumIssues'>200</ac:parameter>
<ac:parameter ac:name='server'>System JIRA</ac:parameter>
<ac:parameter ac:name='columns'>key,summary,type,created,updated,due,assignee,reporter,severity,status,resolution</ac:parameter>
<ac:parameter ac:name='jqlQuery'>project = '$jira_project_key' AND issuetype = Bug AND status not in (Resolved, Accepted) ORDER BY priority DESC, updated DESC</ac:parameter>
<ac:parameter ac:name='serverId'>$atlassian_jira_server</ac:parameter>
</ac:structured-macro>
</p><p> </p>"

# clean body to be a proper data
confluence_body=$(echo $confluence_body | tr -d '\n')
confluence_query="$confluence_url/content/$confluence_continuous_page_id"
confluence_data='{"id":"'$confluence_continuous_page_id'","type":"page","title":"'$confluence_continuous_page_name'","body":{"storage":{"value":"'$confluence_body'","representation":"storage"}},"version":{"number":'$page_version'}}'
query_result=$(curl --silent -u $atlassian_login:$atlassian_password -X PUT -H 'Content-Type: application/json' -d "$confluence_data" $confluence_query)
