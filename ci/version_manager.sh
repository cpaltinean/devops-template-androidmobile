#!/usr/bin/env bash

# usage: ./version_manager.sh Try to select the relevant version and try to increments the relevant version part by one.
# usage: ./version_manager.sh <major|release|hotfix|develop> - Increments the relevant version part by one.
# usage: ./version_manager.sh X.X.X.X - Set the active version to the new version

# NOTE: version value will be calculate (start from X.Y.Z.T) like: X * 1000000000 + Y * 1000000 + Z * 1000 + T
set -e

function version_manager()
{
  echo "CI_BUILD_REF_NAME"
  echo $CI_BUILD_REF_NAME

  echo "CI_COMMIT_REF_NAME"
  echo $CI_COMMIT_REF_NAME

  command=$secondParameter
  current_version=`perl -nle'print $& while m{(?<="version": ")[^"]*}g' ci/version_keeper.json`

  # we don't have anything - we try to get branch name and relevant version.
  if [ "$command" == "" ]; then
    #	active_branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
    active_branch=$CI_COMMIT_REF_NAME
    if [[ $active_branch == "release/"* ]]; then #release
      command="release"
    else
      if [[ $active_branch == "hotfix/"* ]]; then
        command="hotfix"
      else
        if [[ $active_branch == "develop" ]]; then
          command="develop"
        else
          if [[ $active_branch =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
            command="tag"
          else
            echo >&2 "The new version doesn't look like a valid version tag (e.g: 1.2.3.4). Aborting."
            exit 1
          fi
        fi
      fi
    fi
  fi

  echo $command

  if [ "$1" == "value_only" ]; then
    echo "value only"
  else
    echo "no param"
  fi

  # we have relevant version so increments part by one.
  if [ "$command" == "major" ] || [ "$command" == "release" ] || [ "$command" == "hotfix" ] || [ "$command" == "develop" ] || [ "$command" == "tag" ]; then
    IFS='.' read -a version_parts <<< "$current_version"

    major=${version_parts[0]}
    release=${version_parts[1]}
    hotfix=${version_parts[2]}
    develop=${version_parts[3]}

    case "$command" in
      "major")
        major=$((major + 1))
        release=0
        hotfix=0
        develop=0
        ;;
      "release")
        active_branch=$CI_COMMIT_REF_NAME
        release_branch_nr=${active_branch#*"release/"}
        if [ "$active_branch" == "$release_branch_nr" ]; then
          echo "Active branch is not a release branch."
          exit 1
        else
          IFS='.' read -a rbnr_parts <<< "$release_branch_nr"
          release_part=${rbnr_parts[1]}
          if [ "$release" == "$release_part" ]; then
            hotfix=$((hotfix + 1))
            develop=0
          else
            release=$((release + 1))
            hotfix=0
            develop=0
          fi
        fi
        ;;
      "hotfix")
        hotfix=$((hotfix + 1))
        develop=0
        ;;
      "develop")
        develop=$((develop + 1))
        ;;
      "tag")
        hotfix=$((hotfix + 1))
        develop=0
        ;;
    esac

    if [ "$1" == "value_only" ]; then
      new_version=$current_version
    else
      new_version="$major.$release.$hotfix.$develop"
    fi
    new_version_value=$(($major*1000000000+$release*1000000+$hotfix*1000+$develop))

  # we have a possible version
  else
    new_version="$command"
    if ! [[ "$new_version" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
      echo >&2 "The new version doesn't look like a valid version tag (e.g: 1.2.3.4). Aborting."
      exit 1
    fi

    IFS='.' read -a version_parts <<< "$new_version"

    major=${version_parts[0]}
    release=${version_parts[1]}
    hotfix=${version_parts[2]}
    develop=${version_parts[3]}

    if [ "$1" == "value_only" ]; then
      new_version=$current_version
    else
      new_version="$major.$release.$hotfix.$develop"
    fi
    new_version_value=$(($major*1000000000+$release*1000000+$hotfix*1000+$develop))
  fi

  # set the "build.gradle" path (need to take in consideration the PROJECT_PATH)
  if [ -z "$PROJECT_PATH" ]
  then
    gradePath="build.gradle"
  else
    gradePath="$PROJECT_PATH/build.gradle"
  fi

  perl -pi -e 's/VERSION_CODE=.*/VERSION_CODE= '$new_version_value'/g' $gradePath
  perl -pi -e 's/VERSION_NAME=.*/VERSION_NAME= "'$new_version'"/g' $gradePath

  perl -pi -e 's/"version": "'$current_version'"/"version": "'$new_version'"/g' ci/version_keeper.json
}
