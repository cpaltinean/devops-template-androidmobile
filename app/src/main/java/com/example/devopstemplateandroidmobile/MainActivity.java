package com.example.devopstemplateandroidmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.crashBtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Force a crash
                throw new RuntimeException(String.valueOf(R.string.test_crash));
            }
        });
    }
}
